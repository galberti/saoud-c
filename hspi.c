
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <pthread.h>

#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>

#define MCAST_PORT 47781
#define MCAST_GROUP "239.47.78.1"

#define BUFLEN 1024
#define DEFSERV "127.0.0.1"
#define DEFSQUELCH 0.0001
#define PORT 47308

typedef struct {
    double Frequency;
    uint32_t WaitForThisSample;
} samplerf_t;

struct timeval laststime;
FILE *tx = NULL;
pthread_mutex_t lock;
struct sockaddr_in servaddr;
unsigned int saddr_len;
int sock;
unsigned char debug;
unsigned char signal;
float squelch;
float tx_f;
float rx_f;
float ppm;

#define SAMPLERATE 1200000
#define TXF 432800
#define RXF 433800
#define PPM -45

#define DECIMATION SAMPLERATE/48000
#define DECIM_BAND 12000.0/SAMPLERATE
// bandpass_fir_fft_cc filters -12kHz +12kHz, 2400Hz transition

#define RXCOMMAND "rtl_sdr -s %d -p %f -f %f -g 20 - " \
    "| csdr convert_u8_f " \
    "| csdr shift_addition_cc %f " \
    "| csdr fir_decimate_cc %d %f BLACKMAN " \
    "| csdr squelch_and_smeter_cc --fifo /tmp/sq --outfifo /tmp/sm 10 1 " \
    "| csdr bandpass_fir_fft_cc -0.25 0.25 0.05 BLACKMAN " \
    "| csdr fmdemod_quadri_cf " \
    "| csdr limit_ff " \
    "| csdr deemphasis_nfm_ff 48000 " \
    "| csdr gain_ff 3 " \
    "| csdr convert_f_s16"
//  "| csdr bandpass_fir_fft_cc -0.16 0.16 0.05 " \

#define TXCOMMAND "rpitx -i- -m RF -f %f"

int setup_mcast_socket() {
    struct sockaddr_in servaddr;
    struct ip_mreq mreq;
    int s;

    s = socket(PF_INET, SOCK_DGRAM, 0);
    if(s == -1){
        return -1;
    }

    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = PF_INET;
    servaddr.sin_addr.s_addr = INADDR_ANY;
    servaddr.sin_port = htons(MCAST_PORT);

    if(bind(s, (const struct sockaddr *)&servaddr, sizeof(servaddr)) != 0){
        close(s);
        return -2;
    }

    mreq.imr_multiaddr.s_addr = inet_addr(MCAST_GROUP);
    mreq.imr_interface.s_addr = htonl(INADDR_ANY);

    if(setsockopt(s, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) < 0) {
        close(s);
        return -3;
    }
    return s;
}

void prepare_fifos() {
    mkfifo("/tmp/sq", 0600);
    mkfifo("/tmp/sm", 0600);
}

void write_squelch(float v){
    int sq;
    char squelch[20];
    sprintf(squelch, "%f\n", v);
    sq = open("/tmp/sq", O_WRONLY);
    if(sq == 0){
        perror("sq fifo");
        return;
    }
    write(sq, squelch, strlen(squelch));
    printf("squelch written\n");
    close(sq);
}

void *smthread(void *v) {
    char b[80];
    int sm, r;
    sm = open("/tmp/sm", O_RDONLY);
    if(sm == 0){
        perror("sm fifo");
        return NULL;
    }
    printf("main loop smeter read\n");
    while(1){
        r = read(sm, b, 80);
        b[r] = '\0';
        // b already contains \n
        if(signal)
            printf("signal %s", b);
    }
}

void *rxthread(void *v) {
    FILE *rx;
    char rxb[960];
    int r;
    char rxcommand[1024];

    // baseband freq
    float bb = (rx_f - 300) * 1000;
    // shift in percentage, to recenter signal on rx_f
    float shift = (bb - (rx_f * 1000)) / SAMPLERATE;

    sprintf(rxcommand, RXCOMMAND,
        SAMPLERATE, ppm, bb,
        shift,
        DECIMATION, DECIM_BAND);
    rx = popen(rxcommand, "r");
    if(rx == NULL){
        perror("popen()");
        return NULL;
    }
    write_squelch(squelch);
    // pre-fill packet header
    // rxb[0] = 0x08;
    // rxb[1] = 0x07;
    // rxb[2] = 0x12;
    // rxb[3] = 0xc0;
    // rxb[4] = 0x07;
    printf("loop rxthread\n");
    while(1) {
        r = fread(rxb, 1, 960, rx);
        if(r == 0){
            perror("fread()");
            continue;
        }
        if(r < 960){
            perror("fread() too short");
            continue;
        }
        // I get zeroes if signal < squelch - let's check the first 3
        if(rxb[0] != 0 && rxb[1] != 0 && rxb[2] != 0){
            sendto(sock, rxb, 960, 0, (struct sockaddr *)&servaddr, saddr_len);
        }
    }
    return NULL;
}

// tx tmout thread
void *tmthread(void *v) {
    struct timeval now;
    samplerf_t sout;
    int i;
    int o;
    bzero(&sout, sizeof(samplerf_t));
    sout.WaitForThisSample = 20833;
    while(1) {
        gettimeofday(&now, NULL);
        pthread_mutex_lock(&lock);
        if((tx != NULL) && timercmp(&now, &laststime, >)) {
            pthread_mutex_unlock(&lock);
            if(debug){
                printf("tx off\n");
            }
            // 200 ms silence
            for(i = 0; i < 480*20; i++){
                sout.Frequency = 0;
                fwrite(&sout, sizeof(samplerf_t), 1, tx);
            }
            pthread_mutex_lock(&lock);
            o = pclose(tx);
            if(o == -1){
                perror("pclose()");
            }
            tx = NULL;
        }
        pthread_mutex_unlock(&lock);
        usleep(50000);
    }
    return NULL;
}

int main(int argc, char **argv) {
    char buf[BUFLEN];
    char *srv = NULL;
    int n, i;
    samplerf_t sout;
    short sample;
    pthread_t thread_tx, thread_rx, thread_sm;
    struct timeval now, delta;
    char rpitxcmd[160];

    squelch = DEFSQUELCH;
    tx_f = TXF;
    rx_f = RXF;
    ppm = PPM;
    i = 1;
    while(i < argc){
        if(strncmp(argv[i], "-d", 2) == 0){
            debug = 1;
            i++;
            continue;
        }
        if(strncmp(argv[i], "-s", 2) == 0){
            i++;
            srv = argv[i++];
            continue;
        }
        if(strncmp(argv[i], "-b", 2) == 0){
            signal = 1;
            i++;
            continue;
        }
        if(strncmp(argv[i], "-h", 2) == 0){
            printf("%s [-d] [-b] [-s <server>] [-q <squelch>(0.0005)]\n"
                "    [-t <tx_freq>(432800)]\n"
                "    [-r <rx_freq>(433800)]\n"
                "    [-k <ppm>(-45)]\n",
            argv[0]);
            i++;
            return 1;
        }
        if(strncmp(argv[i], "-q", 2) == 0){
            // ~0.1 seems to be the max (0 dB?)
            // 0.0005 is -23 dB
            // 0.0001 is default (-30 dB)
            i++;
            if(sscanf(argv[i++], "%f", &squelch) != 1){
                printf("-q wrong, applying default squelch");
            }
            continue;
        }
        if(strncmp(argv[i], "-r", 2) == 0){
            i++;
            if(sscanf(argv[i++], "%f", &rx_f) != 1){
                printf("-r wrong, applying default rx freq");
            }
            continue;
        }
        if(strncmp(argv[i], "-t", 2) == 0){
            i++;
            if(sscanf(argv[i++], "%f", &tx_f) != 1){
                printf("-t wrong, applying default tx freq");
            }
            continue;
        }
        if(strncmp(argv[i], "-k", 2) == 0){
            i++;
            if(sscanf(argv[i++], "%f", &ppm) != 1){
                printf("-k wrong, applying default ppm");
            }
            continue;
        }

    }
    if(srv == NULL)
        srv = DEFSERV;

    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(PORT);
    if(inet_pton(AF_INET, srv, &servaddr.sin_addr) <= 0) {
        perror("inet_pton()");
        return -1;
    }

    sock = setup_mcast_socket();
    if(sock < 0){
        printf("setup_mcast_socket(): %d\n", sock);
        return -2;
    }

    if (pthread_mutex_init(&lock, NULL) != 0) { 
        perror("pthread_mutex_init()"); 
        return -3; 
    }

    prepare_fifos();
    pthread_create(&thread_rx, NULL, rxthread, NULL);
    pthread_create(&thread_sm, NULL, smthread, NULL);
    pthread_create(&thread_tx, NULL, tmthread, NULL);
    saddr_len = sizeof(servaddr);
    bzero(&sout, sizeof(samplerf_t));
    bzero(&laststime, sizeof(struct timeval));
    // t between samples in ns - 1e9/48000
    sout.WaitForThisSample = 20833;
    delta.tv_sec = 0;
    delta.tv_usec = 150000;
    sprintf(rpitxcmd, TXCOMMAND, tx_f);
    while(1){
        n = recvfrom(sock, buf, BUFLEN, MSG_WAITALL, 
            (struct sockaddr *)&servaddr, &saddr_len);
        if(n < 0){
            perror("recvfrom()");
            continue;
        }
        if(n != 960){
            printf("warning: received %d bytes instead of 960\n", n);
            continue;
        }
        gettimeofday(&now, NULL);
        pthread_mutex_lock(&lock);
        timeradd(&now, &delta, &laststime);
        if(tx == NULL){
            if(debug){
                printf("TX - launching rpitx\n");
            }
            tx = popen(rpitxcmd, "w");
            if(tx == NULL){
                perror("popen()");
                return -4;
            }
        }

        for(i = 0; i < 480; i++){
            sample = (buf[(2*i)] << 8) | buf[(2*i)+1];
            // float must be fraction of short max value (..)
            // amplify 6000 times
            sout.Frequency = ((double)sample/32767*6000);
            fwrite(&sout, sizeof(samplerf_t), 1, tx);
        }
        pthread_mutex_unlock(&lock);
    }
}

