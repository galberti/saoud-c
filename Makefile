
LDFLAGS = -lpthread

all: hspi

hspi: hspi.c
	cc $< $(LDFLAGS) -o $@

clean:
	rm -f hspi

