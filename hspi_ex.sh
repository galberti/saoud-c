#!/bin/bash -x

DIR=/home/pi/
PASS=""

while :
do
    PIDH=$(pidof hspi)
    PIDS=$(pidof saoud)
    RTL=$(lsusb | grep RTL)
    if [ -n "$RTL" ] && ( [ -z "$PIDH" ] || [ -z "$PIDS" ] )
    then
        ping -c 4 www.google.com 2> /dev/null
        DG=$?
        if [ "z$DG" == "z0" ]
        then
            # if hspi dies, second saoud will not run (bind error)
            screen -dm -S saoud $DIR/go/bin/saoud -h antani.uk.to -p $PASS -l
            sleep 1
            screen -S saoud -X screen $DIR/saoud-c/hspi
        fi
    fi
    if [ -z "$RTL" ] && [ -n "$PIDH" ]
    then
        kill $PIDH
        kill $PIDS
    fi
    sleep 2
done

